import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Inicio from './component/inicio';
import InicioSesion from './component/inicioSesion';
import Registro from './component/Registro';
import './bootstrap.min.css';

function App() {

  const [usuario, guardarUsuario] = useState(true);
  return (
    <Router>
      <Switch>
        <Route exact path="/registro-usuario"
          render={() => (
            <Registro
              guardarUsuario={guardarUsuario}
            />
          )}
        />
        <Route exact path="/"
          render={() => (
            <InicioSesion
            guardarUsuario={guardarUsuario}
            />
          )}
        />
        <Route exact path="/inicio"
          render={() => (
            <Inicio
            />
          )}
        />
      </Switch>
    </Router>
  );
}
export default App;