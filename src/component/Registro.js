import React, { useState } from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import Swal from 'sweetalert2';

function Registro({ history, guardarUsuario }) {

    //inicializamos las variables
    const [username, guardarusername] = useState('');
    const [email, guardaremail] = useState('');
    const [telefono, guardartelefono] = useState('');
    const [password, guardarpassword] = useState('');
    const [error, guardarerror] = useState(false);

    const handleSubmit = async e => {
        e.preventDefault();

        //validar que los campos no esten vacios
        if (username === '' || email === '' || telefono === '' || password === '') {
            guardarerror(true);
            return
        }
        guardarerror(false);
        //guardar usuarios
        try {
            const registro = await axios.post('http://localhost:8080/user/new', {
                username,
                email,
                telefono,
                password
            });
            if (registro.data.status === 200) {
                const inicio = await axios.get('http://localhost:8080/user', {
                    params: {
                        username: username,
                        password: password
                    }
                });
                localStorage.setItem('idUser', inicio.data.user.id);
                history.push('/inicio');

            } else {
                console.log(registro.data.status);
                Swal.fire(
                    'Lo sentimos',
                    'El usuario ya existe',
                    'error'
                )
            }
        } catch (error) {
            console.log(error);
            Swal.fire({
                type: 'error',
                title: 'Error',
                text: 'Hubo un error, vuelve a intentarlo'
            })
        }
    }
    return (
        <div className="card caja-inicio mt-5 py-5" >
            <div className="card-body">
                <h2 className="card-title text-center mb-5">
                    Registro de usuarios
                    </h2>
                {(error) ? <div className="alert alert-danger mt-2 mb-5 text-center">Todos los campos son obligatorios</div> : null}
                <form onSubmit={handleSubmit}>
                    <div className="form-group row">
                        <label className="col-sm-4 col-lg-2 col-form-label">Usuario</label>
                        <div className="col-sm-8 col-lg-10">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Usuario"
                                name="username"
                                onChange={e => guardarusername(e.target.value)} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-lg-2 col-form-label">Email</label>
                        <div className="col-sm-8 col-lg-10">
                            <input
                                type="email"
                                className="form-control"
                                placeholder="Email"
                                name="email"
                                onChange={e => guardaremail(e.target.value)} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-lg-2 col-form-label">Telefono</label>
                        <div className="col-sm-8 col-lg-10">
                            <input
                                type="number"
                                className="form-control"
                                placeholder="Numero de telefono"
                                name="telefono"
                                onChange={e => guardartelefono(e.target.value)} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-lg-2 col-form-label">Contraseña</label>
                        <div className="col-sm-8 col-lg-10">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Contraseña"
                                name="password"
                                onChange={e => guardarpassword(e.target.value)} />
                        </div>
                    </div>
                    <input type="submit" className="py-3 mt-2 btn btn-success btn btn-block" value="Registrar" />
                </form>
            </div>
        </div>
    )
}

export default withRouter(Registro);