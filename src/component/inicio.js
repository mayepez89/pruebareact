import React from 'react';
import axios from 'axios';
import Swal from 'sweetalert2';
import InfiniteScroll from "react-infinite-scroll-component";
import { withRouter } from 'react-router-dom';

class Inicio extends React.Component {
    state = {
        personajes: [],
        pageNumber: 1,
        items: 15,
        hasMore: true
    };

    componentDidMount() {
        //initial request is sent
        this.fetchData();
    }

    fetchData = () => {
        const usuario = localStorage.getItem('idUser');
        const url = 'http://localhost:8080/mostrar/' + usuario + '/';
        axios
            .get(url + this.state.pageNumber)
            .then(res => {
                console.log(res)
                if (res.data.status === 200) {
                    this.setState({
                        //updating data
                        personajes: [...this.state.personajes, ...res.data.data.results],
                        //updating page numbers
                        pageNumber: this.state.pageNumber + 1
                    })
                } else {
                    localStorage.removeItem('idUser');
                    Swal.fire({
                        type: 'error',
                        title: 'Lo sentimos',
                        text: 'Tu sesion caducó',
                        confirmButtonText: 'Entendido',
                    }).then((result) => {
                        if (result.value) {
                            this.props.history.push('/');
                        }
                      })
                }
            }
            );
    };

    render() {
        return (
            <div className="card mt-5 py-5" >
                <div className="card-body">
                    <h2 className="card-title text-center mb-5">
                        Listado de Personajes
                    </h2>
                    <InfiniteScroll
                        dataLength={this.state.personajes.length} //This is important field to render the next data
                        next={this.fetchData}
                        hasMore={this.state.hasMore}
                        loader={<h4>Cargando...</h4>}
                    >
                        <div className="row">
                            {this.state.personajes.map(per => (
                                <div className="col" key={per.name}>
                                    <div className="card-header"><b>Nombre: </b>{per.name}</div>
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col">
                                                <img src={per.image}></img>
                                            </div>
                                            <div className="col">
                                                <p className="card-text"><b>Genero: </b>{per.gender}</p>
                                                <p className="card-text"><b>Estatus: </b>{per.status}</p>
                                                <p className="card-text"><b>Especie: </b>{per.species}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </InfiniteScroll>
                </div>
            </div>
        );
    }
}
export default withRouter(Inicio);