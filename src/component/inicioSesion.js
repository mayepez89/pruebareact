import React, { useState } from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';

function InicioSesion({history,guardarUsuario}) {

    //inicializamos las variables
    const [username, guardarusername] = useState('');
    const [password, guardarpassword] = useState('');
    const [error, guardarerror] = useState(false);

    const handleSubmit = async e => {
        e.preventDefault();

        //validar que los campos no esten vacios
        if (username === '' || password === '') {
            guardarerror(true);
            history.push('/inicio');
            return
        }
        guardarerror(false);
        //iniciar sesion
        try {
            const inicio = await axios.get('http://localhost:8080/user', {
                params: {
                    username: username,
                    password: password
                }
            });
            console.log(inicio);
            if (inicio.data.status === 200) {
                localStorage.setItem('idUser', inicio.data.user.id);
                history.push('/inicio');
            } else {
                Swal.fire(
                    'Lo sentimos',
                    'Usuario invalido',
                    'error'
                )
            }
        } catch (error) {
            console.log(error);
            Swal.fire({
                type: 'error',
                title: 'Error',
                text: 'Hubo un error, vuelve a intentarlo'
            })
        }
    }

    return (
        <div className="card caja-inicio mt-5 py-5">
            <div className="card-body">
                <h2 className="card-title text-center mb-5">
                    Inicio de sesion
                </h2>
                {error ? <div className="alert alert-danger mt-2 mb-5 text-center">Todos los campos son obligatorios</div> : null}
                <form onSubmit={handleSubmit}>
                    <div className="form-group row">
                        <label className="col-sm-4 col-lg-2 col-form-label">Usuario:</label>
                        <div className="col-sm-8 col-lg-10">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Usuario"
                                name="username"
                                onChange={e => guardarusername(e.target.value)} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-lg-2 col-form-label">Password:</label>
                        <div className="col-sm-8 col-lg-10">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Password"
                                name="password"
                                onChange={e => guardarpassword(e.target.value)} />
                        </div>
                    </div>
                    <br/>
                    <h6 className="text-center">Si no estas registrado. 
                    <Link to="/registro-usuario">
                    Registrate AQUI
                    </Link>
                    </h6>
                    <br/>
                    <input type="submit" className="py-3 mt-2 btn btn-success btn btn-block" value="Ingresar" />
                </form>
            </div>
        </div>
    );
}
export default withRouter(InicioSesion);